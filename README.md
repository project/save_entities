CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers


INTRODUCTION
------------

The Save Entities (save_entities) module provides a simple way for users to
save nodes and media in bulk using a form. Besides the ability of choosing
which content types or media types to save, the module also provides the option
of saving only published content and to update the changed date of the entities.

REQUIREMENTS
------------

This module requires no modules outside of Drupal core.


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.


CONFIGURATION
-------------

 * Two forms are provided, one for nodes and one for media.
 The forms can be found in:

   Administration » Configuration » Content Authoring

   After choosing the form:

   - Select which entities should be saved and which options should be checked;

   - Click Save.

MAINTAINERS
-----------

Current maintainers:
 * Nelson Alves (nelson-alves) - https://www.drupal.org/u/nelson-alves

This project has been sponsored by:
 * NTT DATA
   NTT DATA – a part of NTT Group – is a trusted global innovator of IT and business services headquartered in Tokyo.
   NTT is one of the largest IT services provider in the world and has 140,000 professionals, operating in more than 50 countries.
   NTT DATA supports clients in their digital development through a wide range of consulting and strategic advisory services, cutting-edge technologies, applications, infrastructure, modernization of IT and BPOs.
   We contribute with vast experience in all sectors of economic activity and have extensive knowledge of the locations in which we operate.
